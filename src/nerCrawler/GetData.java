package nerCrawler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.KeepEverythingExtractor;

public class GetData {

	String socialLink="";
	String emailId = "";
	String Typelink="";
	String phone = "";
	String seedUrl = "",doc="";

	public Document getJsoupDoc(String url)
	{
		Document document = null;

		try {
			document = Jsoup.connect(url).timeout(10000).get();
			//return document;	
		} catch (IOException e) {

			//System.out.println("Link is not supported by system........");
			//e.printStackTrace();
		}
		catch(Exception e)
		{
			//System.out.println("Link is not compatible");
		}

		return document; 
	}

	public String getCompany(String url)
	{

		String tempurl=url;
		//If www is not present in URL
		if(!tempurl.contains("www")){
			tempurl=tempurl.replaceAll("https://", "");
			tempurl=tempurl.replaceAll("http://", "");
			tempurl = "https://wwww."+tempurl;
		}

		String domainName = tempurl.substring(tempurl.indexOf(".")+1, tempurl.length());

		String companyName = domainName.substring(0, domainName.indexOf(".")).toLowerCase();

		//System.out.println(companyName);

		return companyName;
	}


	public String getSocialLink(Document doc,String regex)
	{
		socialLink ="";
		Elements links = doc.select("a[href]");
		for(Element link:links)
		{
			if(link.absUrl("abs:href").toLowerCase().matches(regex))//get the data pinterest link
			{
				if(!link.absUrl("abs:href").toLowerCase().contains("/share"))
				{
					socialLink = link.attr("abs:href"); //add the pinterest link
					break;
				}
			}
		}
		return socialLink;
	}

	public String getTypeLink(Document doc, String parallelWords, String parallelTitle)
	{

		Typelink = "";
		Elements links = doc.select("a[href]");
		String words[] = parallelWords.split(",");
		String titles[] = parallelTitle.split(",");
		boolean flag = false;
		for(Element link:links)
		{
			if(flag==true)
			{
				break;
			}
			String linkTest = link.absUrl("abs:href").toLowerCase();
			for(String word : words)
			{
				if(flag==true)
				{
					break;
				}
				int i= 0;
				//System.out.println(word);
				if(linkTest.contains(word)) 
				{	
					while(i<titles.length)//System.out.println("word"+word);
					{
						if(link.text().toLowerCase().matches("(\\D*\\s)*"+titles[i]+"(\\s\\D*)*"))
						{
							//System.out.println(titles[i]); 
							Typelink = link.attr("abs:href");  //add the  Typelink here
							flag = true;
							break;
						}
						i++;
					}
				}

			}
		}

		return Typelink;
	}
	public String getSeedUrl(Document doc, String parallelWords,String parallelTitle)
	{
		seedUrl ="";
		Elements links = doc.select("a[href]");
		String words[] = parallelWords.split(",");
		String titles[] = parallelTitle.split(",");
		boolean flag = false;
		for(Element link : links) 
		{

			String linkTest = link.absUrl("abs:href").toLowerCase();//get the contact-us page here 
			for(String word : words)
			{
				int i= 0;
				if(linkTest.contains(word) || link.text().toLowerCase().contains(titles[i]) && i<titles.length) //|| linkTest.contains("say-hi") || linkTest.contains("reach"))
				{
					//|| link.text().toLowerCase().contains("reach"))
					//System.out.println("link : " +link.absUrl("abs:href"));
					seedUrl = link.absUrl("abs:href");
					flag = true;
					break;
				}
				i++;

			}
			if(flag==true)
			{
				break;
			}
		}
		return seedUrl;
	}

	public String getBoiler(String url)
	{
		String textToProcess = "";
		URL urlToProcess;
		try {
			urlToProcess = new URL(url);
			//System.out.println("Inside boilerpipe url to process:"+urlToProcess);
			textToProcess=KeepEverythingExtractor.INSTANCE.getText(urlToProcess);

		} catch (MalformedURLException | BoilerpipeProcessingException e) {

			//e.printStackTrace();
		}

		return textToProcess;

	}

	public String getEmailId(String textProcess)
	{

		Set<String> email = new HashSet<String>();
		Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(textProcess);
		
		if(!email.isEmpty()) //if emailId is present
		{
			for(String e1:email)
			{

				if(!e1.equals("")) // checking emailID if it null
				{
					emailId+=e1+",";
					//System.out.println("inside"+emailId);
				}
			}
			if(!emailId.isEmpty()){
				emailId = emailId.substring(0,emailId.length()-1); //Remove the last comma(,)
			}
		}
		//System.out.println("!!!!!!!!!!!!!!!!!"+emailId);
		return emailId;
	}

	public String getPhone(String textProcess)
	{
		Set<String> phn = new HashSet<String>();
		Matcher m1 = Pattern.compile("\\D*:?\\+?[ 0-9. ()-]{11,25}").matcher(textProcess);
		while (m1.find()) {

			//	phn.add(m1.group().replaceAll("[a-zA-Z<>,+:\\\\/';=_!.& ()#}\\-\"]","").trim());
			String num=m1.group().replaceAll("[^0-9]","").trim();// Remove the starting and ending spaces only take phone number others are remove like(,.;:'"?/()*+-_%$#!=))
			if(num.length() < 25 && num.length() >= 10) //constraints for phone number
			{
				//System.out.println("No. :"+num);
				phn.add(num);
			}
		}
		if(!phn.isEmpty()) //if the phone number is present
		{
			for(String p:phn)
			{
				//System.out.println("hhhhhhiiii"+p);
				phone+=p+","; 
			}
			phone= phone.substring(0, phone.length()-1);  //Remove the last comma(,)

		}	
		return phone;
	}
}


