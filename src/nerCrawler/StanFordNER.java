package nerCrawler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;

public class StanFordNER {
	static String content;
	static String serializedClassifier;
	static AbstractSequenceClassifier<CoreLabel> classifier = null;
	String date, location, organization, person, percent, time, money;
	static {
		serializedClassifier = "classifiers/english.muc.7class.distsim.crf.ser.gz";

		try {
			classifier = CRFClassifier.getClassifier(serializedClassifier);
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public StanFordNER() {

	}

	public HashMap<String, LinkedList<String>> getDataForAllClasses(
			String content) {
		HashMap<String, LinkedList<String>> hm = new HashMap<String, LinkedList<String>>();
		LinkedList<String> locationAl = new LinkedList<String>();
		LinkedList<String> organizationAl = new LinkedList<String>();
		LinkedList<String> dateAl = new LinkedList<String>();
		LinkedList<String> moneyAl = new LinkedList<String>();
		LinkedList<String> personAl = new LinkedList<String>();
		LinkedList<String> percentAl = new LinkedList<String>();
		LinkedList<String> timeAl = new LinkedList<String>();

		// List<List<CoreLabel>> out = classifier.classify(content);
		List<Triple<String, Integer, Integer>> list = classifier
				.classifyToCharacterOffsets(content);
		for (Triple<String, Integer, Integer> item : list) {
			// System.out.println(item);
			// System.out.println(item.first() + ": " +
			// content.substring(item.second(), item.third()));
			if (item.first().contains("DATE")) {
				date = content.substring(item.second(), item.third());
				dateAl.add(date);
			} else if (item.first().contains("LOCATION")) {
				location = content.substring(item.second(), item.third());
				locationAl.add(location);
			} else if (item.first().contains("ORGANIZATION")) {
				organization = content.substring(item.second(), item.third());
				organizationAl.add(organization);
			} else if (item.first().contains("PERSON")) {
				person = content.substring(item.second(), item.third());
				personAl.add(person);
			} else if (item.first().contains("PERCENT")) {
				percent = content.substring(item.second(), item.third());
				percentAl.add(percent);
			} else if (item.first().contains("TIME")) {
				time = content.substring(item.second(), item.third());
				timeAl.add(time);
			} else if (item.first().contains("MONEY")) {
				money = content.substring(item.second(), item.third());
				moneyAl.add(money);
			}

		}
		hm.put("location", locationAl);
		hm.put("organization", organizationAl);
		hm.put("date", dateAl);
		hm.put("money", moneyAl);
		hm.put("person", personAl);
		hm.put("percent", percentAl);
		hm.put("time", timeAl);
		return hm;

	}

	public static void main(String s[]) {
		StringBuilder sbr = new StringBuilder();
		for (int i = 0; i < s.length; i++) {
			sbr.append(s[i] + " ");
		}
		// System.out.println(sbr.toString());
		// String str="my name is ruksad, my name is ruksad";
		StanFordNER stanford = new StanFordNER();
		HashMap hm = stanford.getDataForAllClasses(sbr.toString());

		Set set = hm.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, LinkedList<String>> me = (Entry<String, LinkedList<String>>) iterator
					.next();
			String key = me.getKey();
			System.out.print(key + "= ");
			LinkedList<String> ll = me.getValue();
			for (String l : ll) {
				System.out.print(l + " ,");
			}
			System.out.println();
		}
	}
}
