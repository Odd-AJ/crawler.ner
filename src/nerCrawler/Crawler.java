package nerCrawler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.nodes.Document;

import nerCrawler.GetData;
import scala.collection.generic.BitOperations.Int;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

import geoLocation.GeoLocation;
import mongoDBConnection.GetConnection;

public class Crawler {

	private static final int MYTHREADS = 3;
	static int count = 1;
	private static long startTime;
	static DBCollection crawldatadb;
	static DBCollection sampleCrawlDataDB;
	static DBCollection domainDB;
	static DBCollection settingsDB;
	static GetData getData;
	static DBCollection dataCountDB;
	// static DBCollection seedUrl = crawler.getCollection("SeedUrl");
	// static DBCollection name = crawler.getCollection("nameStructure");
	// static DBCollection newdb = crawler.getCollection("newdb");
	static BasicDBObject insertObject = new BasicDBObject();

	
	static{
		startTime = System.currentTimeMillis();
		crawldatadb = GetConnection.connect("crawldata");
		sampleCrawlDataDB = GetConnection.connect("sampleCrawldata");
		domainDB = GetConnection.connect("domain");
		settingsDB = GetConnection.connect("settings");
		getData = new GetData();
		dataCountDB = GetConnection.connect("dataCount");
	}
	
	static localData data;
	
	public void crawlSeeds(ArrayList<localData> list) {
		ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Seed is " + list.get(i).url);

			String url = list.get(i).url;
			boolean status = list.get(i).status;
			String personName = list.get(i).personName;
			String mainContent = list.get(i).mainContent;
			String subCategory = list.get(i).subCategory;
			String systemName = list.get(i).systemDate;
			String moduleName = list.get(i).moduleName;
			int id = list.get(i).id;
			Runnable worker = new MyRunnable(id, url, status,personName,mainContent, subCategory, systemName, moduleName);
			// executor.submit(worker);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
		System.out.println("\nFinished all threads");

	}

	public static void main(String args[]) throws Exception {

		long startTime = System.currentTimeMillis();
		DBObject object;
		String link;
		String authorityStatus;
		String imageStatus;
		String translationStatus;
		
		String boilerpipeStatus;
		String nerStatus;
		String personName;
		String systemDate;
		String subCategory;
		String moduleName;
		
		int id;
		ArrayList<localData> list = new ArrayList<>();
		ArrayList<localData> crawlList = new ArrayList<>();

		BasicDBObject query = new BasicDBObject();
		query.put("status", "false");
		DBCursor cursor = crawldatadb.find(query);
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		int cursorSize = cursor.size();
		int cursorLimit = 0;
		int batch = 0;
		int i=0;
		if (cursorSize >= 15) {
			System.out.println("Adding " + cursorSize + " data in list..... Please wait for some time");
			while (cursor.hasNext()) {
				object = cursor.next();

				id = Integer.parseInt(object.get("_id").toString());
				link = object.get("link").toString();
				authorityStatus = object.get("authorityStatus").toString();
				imageStatus = object.get("imageStatus").toString();
				translationStatus = object.get("translationStatus").toString();
				
				boilerpipeStatus = object.get("boilerpipeStatus").toString();
				nerStatus = object.get("nerStatus").toString();
				personName = object.get("personName").toString();
				systemDate = object.get("systemDate").toString();
				subCategory = object.get("subCategory").toString();
				moduleName = object.get("moduleName").toString();
				
				String mainContent ="";

				boolean status = false;
				if (translationStatus.contains("true") && authorityStatus.contains("true")) {
					status = true;
				} else {
					status = false;
				}
				
				if(nerStatus.contains("false") && boilerpipeStatus.contains("true"))
				{
					mainContent =  object.get("mainContent").toString();
					data = new localData(id, link, status, personName, mainContent, systemDate, subCategory, moduleName);
					crawlList.add(data);
				}
				else
				{
					continue;
				}
			}
			cursor.close();
		}

		if (crawlList.size() < 15) {
			System.out.println("Data is less than 15 to fetch NER");
			System.out.println("Wait for 2 minutes to get more than 15 data to start NER crawl");
			System.gc();
			Thread.sleep(120000); // 2 minutes in milliseconds
		} else {
			while (cursorLimit != crawlList.size()) {
				if (batch < 15) {
					list.add(crawlList.get(cursorLimit));
					cursorLimit++;
					batch++;
				} else {
					System.out.println("List is 15 and calling threads");
					new Crawler().crawlSeeds(list);
					System.out.println("Now list is clear");
					list.clear();
					batch = 0;
				}
			}
			if(list.size()>0)
			{
				System.out.println("Remaining links are deployed to Threads");
				new Crawler().crawlSeeds(list);
				System.out.println("Now list is clear");
				list.clear();
				batch = 0;
			}
		}

		long endTime = System.currentTimeMillis();
		System.out.println("Done with all the execution");
		long time = endTime - startTime;
		System.out.println("Program took " + time + " to execute");
		System.out.println("After half hour program will restart");
		System.gc();
		Thread.sleep(120000); // 2 minutes in milliseconds
		main(args);
		}

	public static class MyRunnable implements Runnable {
		private final String urlToProcess;
		private boolean status;
		private String personName;
		private String content;
		List<String> textsToProcess = new ArrayList<String>();
		Document doc;
		StanFordNER stanford = new StanFordNER();
		private String subCategory, systemName, moduleName;
		int id;
		
		MyRunnable(int id, String urlToProcess, boolean status,String personName, String mainContent, String subCategory, String systemName, String moduleName) {
			this.urlToProcess = urlToProcess;
			this.status = status;
			this.personName = personName;
			this.content = mainContent;
			this.subCategory = subCategory;
			this.systemName = systemName;
			this.moduleName = moduleName;
			this.id = id;
		}

		@Override
		public void run() {
			try {
				BasicDBObject searchQuery = new BasicDBObject("link", urlToProcess);
				
				HashMap hm = stanford.getDataForAllClasses(content);
				String leadDate = hm.get("date").toString();
				String location = hm.get("location").toString();
				String personName1 = hm.get("person").toString();
				
				String companyName = hm.get("organization").toString();

				if (leadDate.length() > 2)
					leadDate = leadDate.substring(1, leadDate.length() - 1);
				else
					leadDate = "";

				if (location.length() > 2)
					location = location.substring(1, location.length() - 1);
				else
					location = "";

				if (personName1.length() > 2)
					personName1 = personName1.substring(1, personName1.length() - 1);
				else
					personName1 = "";

				if(personName.isEmpty())
				{
					personName = personName1;
				}
				else
				{
					personName = personName+personName1;
				}
				
				if (companyName.length() > 2)
					companyName = companyName.substring(1, companyName.length() - 1);
				else
					companyName = "";

				String latlong = "";
				
				if (location != "") {
					latlong = GeoLocation.updateLocation(location);
				}

				// Define the update query:
				
				DBObject dbObject;
				try {
					if (latlong.isEmpty()) {
						dbObject = (DBObject) JSON.parse("{" + "'type' : 'MultiPoint',"
								+ "'coordinates' : [ " + "[ 0, 0 ]" + "]" + "}");
					} else {
						// present.put("keyword", "try");
						//System.out.println("Location::" + latlong);
						// crawldataDB1.update(present, present);
						BasicDBObject doc = new BasicDBObject();
						String l[] = latlong.split("/");

						String query = "'coordinates' : [ ";
						for (int i = 0; i < l.length; i++) {
							String lon[] = l[i].split(",");
							if (i == l.length - 1) {
								query += "[" + lon[1] + "," + lon[0] + "]";
								//System.out.println(lon[1] + " " + lon[0]);
							} else
								query += "[" + lon[1] + "," + lon[0] + "],";
							// doc.putAll();

							// System.out.println("check "+l[i]+" ");
						}
						query += "]";

						dbObject = (DBObject) JSON.parse("{" + "'type' : 'MultiPoint'," + query + "" + "}");
					}
					// }
				} catch (Exception e) {
					dbObject = (DBObject) JSON.parse("{" + "'type' : 'MultiPoint',"
							+ "'coordinates' : [ " + "[ 0, 0 ]" + "]" + "}");
					//System.out.println("My..." + e);

				}
				
				if (status == true) {
					BasicDBObject updateFields = new BasicDBObject();
					updateFields.append("leadDate", leadDate);
					updateFields.append("location", location);
					updateFields.append("geoLocation", latlong);
					updateFields.append("personName", personName);
					updateFields.append("companyName", companyName);
					updateFields.append("status", "true");
					updateFields.append("nerStatus", "true");
					updateFields.append("geoip", dbObject);
					BasicDBObject setQuery = new BasicDBObject();
					setQuery.append("$set", updateFields);
					
					sampleCrawlDataDB.update(searchQuery, setQuery);
					crawldatadb.update(searchQuery, setQuery);
					
					insertObject.append("_id", id);
					insertObject.append("link", urlToProcess);
					insertObject.append("subCategory", subCategory);
					insertObject.append("systemDate", systemName);
					insertObject.append("moduleName", moduleName);
					dataCountDB.insert(insertObject);
					insertObject.clear();
					System.out.println("Done for " + urlToProcess);
				} else {
					BasicDBObject updateFields = new BasicDBObject();
					updateFields.append("leadDate", leadDate);
					updateFields.append("location", location);
					updateFields.append("personName", personName);
					updateFields.append("companyName", companyName);
					updateFields.append("status", "false");
					updateFields.append("nerStatus", "true");
					updateFields.append("geoip", dbObject);
					BasicDBObject setQuery = new BasicDBObject();
					setQuery.append("$set", updateFields);

					sampleCrawlDataDB.update(searchQuery, setQuery);
					crawldatadb.update(searchQuery, setQuery);
					System.out.println("Done for " + urlToProcess);
				}

			}

			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("List of text To Process:"+textsToProcess);
		}

	}

}

class localData {
	public String url;
	public boolean status;
	public String personName;
	public String mainContent, systemDate, subCategory, moduleName;
	int id;
	public localData(int id, String url, boolean status, String personName, String mainContent, String systemDate, String subCategory, String moduleName) {
		super();
		this.url = url;
		this.status = status;
		this.personName=personName;
		this.mainContent = mainContent;
		this.systemDate = systemDate;
		this.subCategory = subCategory;
		this.moduleName = moduleName;
		this.id = id;
	}
}