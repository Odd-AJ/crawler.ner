package geoLocation;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import mongoDBConnection.GetConnection;

public class GeoLocation {
	public static String updateLocation(String alllocation) {
		String allLatlong = "",latlong="";
		try {
			DBCollection geodata=GetConnection.connect("geodata");
			
			Set<String> locationset = new LinkedHashSet<>();
			String[] locations = alllocation.split(",");
			for (int i = 0; i < locations.length; i++) {
				// System.out.println(locations[i]);
				locationset.add(locations[i].trim());
			}
			for (String location : locationset) {
				BasicDBObject query = new BasicDBObject();
				query.put("cityname", Pattern.compile("^" + location + "$", Pattern.CASE_INSENSITIVE));	//parameterized query. here we provide the condition
				DBCursor cursor = geodata.find(query);	//If we don not have query then keep brackets blank. It will fetch all records
				while (cursor.hasNext()) {
				   // System.out.println(cursor.next());
					DBObject present=cursor.next();
					latlong=present.get("latlong").toString();
					allLatlong = allLatlong + latlong + "/";
				}			
				//while (rs.next()) {
					//String latlong = rs.getString("latlong");
					
				//}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return allLatlong;
	}
}
